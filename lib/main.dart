import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:test_paloma365/domain/di/locator.dart';
import 'package:test_paloma365/routes.dart';
import 'package:test_paloma365/ui/resurses/theme.dart';
import 'package:test_paloma365/ui/screens/table/tables_screen.dart';
import 'package:test_paloma365/ui/state_manager/store.dart';

void main() {
  final locator = LocatorService();
  runApp(MyApp(locator: locator));
}

class MyApp extends StatelessWidget {
  final LocatorService locator;

  const MyApp({
    super.key,
    required this.locator,
  });

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: locator.store,
      child: MaterialApp(
        theme: theme,
        navigatorKey: locator.navigatorKey,
        home: const TablesScreen(),
        onGenerateRoute: AppRouter.onGenerateRouters,
      ),
    );
  }
}
