import 'package:test_paloma365/data/models/answer.dart';
import 'package:test_paloma365/data/models/table.dart';

abstract class TableRepository {
  Future<Answer<List<AppTable>>> getListTable();
}
