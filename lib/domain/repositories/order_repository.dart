import 'package:test_paloma365/data/models/order.dart';
import 'package:test_paloma365/data/models/answer.dart';

abstract class OrderRepository {
  Future<Answer<List<Order>>> getListOrder({required int tableNumber});

  Future<void> updateOrder({
    required int productId,
    required int tableNumber,
    required int count,
  });

  Future<Order?> createOrder({
    required int productId,
    required int tableNumber,
  });

  Future<void> deleteOrder({
    required int tableNumber,
    required int productId,
  });
}
