import 'package:test_paloma365/data/models/answer.dart';
import 'package:test_paloma365/data/models/product.dart';

abstract class ProductRepository {
  Future<Answer<List<Product>>> getListProduct();
}
