import 'package:get_it/get_it.dart';
import 'package:test_paloma365/domain/controllers/order_controller.dart';
import 'package:test_paloma365/domain/services/navigator_service.dart';

class GetItServices {
  NavigatorService get navigatorService => GetIt.I.get<NavigatorService>();

  OrderController get orderController => GetIt.I.get<OrderController>();
}

final getItService = GetItServices();
