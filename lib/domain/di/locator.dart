import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:redux/redux.dart';
import 'package:test_paloma365/data/database/database.dart';
import 'package:test_paloma365/data/repositories/api_order_repository.dart';
import 'package:test_paloma365/data/repositories/api_product_repository.dart';
import 'package:test_paloma365/data/repositories/api_table_repository.dart';
import 'package:test_paloma365/domain/controllers/order_controller.dart';
import 'package:test_paloma365/domain/repositories/order_repository.dart';
import 'package:test_paloma365/domain/repositories/product_repository.dart';
import 'package:test_paloma365/domain/repositories/table_repository.dart';
import 'package:test_paloma365/domain/services/navigator_service.dart';
import 'package:test_paloma365/ui/state_manager/order/middleware.dart';
import 'package:test_paloma365/ui/state_manager/order/state.dart';
import 'package:test_paloma365/ui/state_manager/product/middleware.dart';
import 'package:test_paloma365/ui/state_manager/product/state.dart';
import 'package:test_paloma365/ui/state_manager/reducer.dart';
import 'package:test_paloma365/ui/state_manager/store.dart';
import 'package:test_paloma365/ui/state_manager/table/middleware.dart';
import 'package:test_paloma365/ui/state_manager/table/state.dart';

class LocatorService {
  final database = AppDataBase();
  late OrderRepository orderRepository;
  late ProductRepository productRepository;
  late TableRepository tableRepository;

  late OrderController orderController;

  final navigatorKey = GlobalKey<NavigatorState>();
  late NavigatorService navigatorService;
  late Store<AppState> store;

  LocatorService() {
    productRepository = ApiProductRepository();
    orderRepository = ApiOrderRepository(dataBase: database);
    tableRepository = ApiTableRepository(dataBase: database);

    navigatorService = NavigatorService(navigatorKey: navigatorKey);
    store = Store(
      appReducer,
      initialState: AppState(
        productListState: ProductListState(),
        orderListState: OrderListState(),
        tableListState: TableListState(),
      ),
      middleware: [
        OrderMiddleware(orderRepository: orderRepository).call,
        TableMiddleware(tableRepository: tableRepository).call,
        ProductMiddleware(productRepository: productRepository).call,
      ],
    );

    orderController = OrderController(
      orderRepository: orderRepository,
      store: store,
    );

    _register();
  }

  void _register() {
    GetIt.I.registerSingleton<NavigatorService>(navigatorService);
    GetIt.I.registerSingleton<OrderController>(orderController);
  }
}
