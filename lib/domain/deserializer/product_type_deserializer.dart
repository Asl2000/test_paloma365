import 'package:test_paloma365/data/deserializer/deserializer.dart';
import 'package:test_paloma365/domain/enums/product_type.dart';

class ProductTypeDeserializer implements Deserializer<String, ProductType> {
  @override
  String call(ProductType obj) {
    switch (obj) {
      case ProductType.soup:
        return 'Супы';
      case ProductType.hot:
        return 'Горячие блюда';
      case ProductType.sous:
        return 'Соусы';
      case ProductType.desserts:
        return 'Десерты';
      case ProductType.burgers:
        return 'Бургеры';
    }
  }
}
