import 'package:test_paloma365/data/deserializer/deserializer.dart';
import 'package:test_paloma365/domain/enums/product_type.dart';

class ProductTypeEmojiDeserializer implements Deserializer<String, ProductType> {
  @override
  String call(ProductType obj) {
    switch (obj) {
      case ProductType.soup:
        return '🍜';
      case ProductType.hot:
        return '🧆';
      case ProductType.sous:
        return '🧂';
      case ProductType.desserts:
        return '🍰';
      case ProductType.burgers:
        return '🍔';
    }
  }
}
