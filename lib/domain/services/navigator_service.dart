import 'package:flutter/cupertino.dart';
import 'package:test_paloma365/routes.dart';

class NavigatorService {
  final GlobalKey<NavigatorState> navigatorKey;

  NavigatorService({required this.navigatorKey});

  void onPop() => navigatorKey.currentState!.pop();

  void onTableDetail({required int number}) {
    navigatorKey.currentState!.pushNamed(
      AppRouter.tableDetail,
      arguments: AppRouterArguments(number: number),
    );
  }
}
