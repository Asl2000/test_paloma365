import 'package:redux/redux.dart';
import 'package:test_paloma365/data/models/order.dart';
import 'package:test_paloma365/domain/repositories/order_repository.dart';
import 'package:test_paloma365/ui/state_manager/order/action.dart';
import 'package:test_paloma365/ui/state_manager/store.dart';
import 'package:test_paloma365/ui/state_manager/table/action.dart';

class OrderController {
  final OrderRepository orderRepository;
  final Store<AppState> store;

  OrderController({
    required this.orderRepository,
    required this.store,
  });

  Future<void> addProduct({
    required int productId,
    Order? order,
  }) async {
    if (order == null) {
      final newOrder = await orderRepository.createOrder(
          productId: productId, tableNumber: store.state.orderListState.numberTable);
      if (newOrder != null) store.dispatch(AddOrderListAction(order: newOrder));
    } else {
      store.dispatch(UpdateOrderListAction(order: order));
      await orderRepository.updateOrder(
        productId: productId,
        tableNumber: store.state.orderListState.numberTable,
        count: order.count++,
      );
    }
    store.dispatch(LoadTableListAction());
  }

  Future<void> removeProduct({required Order order}) async {
    store.dispatch(UpdateOrderListAction(order: order));
    await orderRepository.updateOrder(
      productId: order.productId,
      tableNumber: store.state.orderListState.numberTable,
      count: order.count--,
    );
    store.dispatch(LoadTableListAction());
  }
}
