import 'dart:io';
import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:path_provider/path_provider.dart';

// ignore: depend_on_referenced_packages
import 'package:path/path.dart' as p;
import 'package:drift/drift.dart' as drift;
import 'package:test_paloma365/data/models/order.dart';
import 'package:test_paloma365/data/database/tables/order_table.dart';
import 'package:test_paloma365/data/models/table.dart';

part 'database.g.dart';

LazyDatabase _openConnection() {
  return LazyDatabase(() async {
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(p.join(dbFolder.path, 'database'));
    return NativeDatabase(file);
  });
}

@DriftDatabase(tables: [OrderTable])
class AppDataBase extends _$AppDataBase {
  AppDataBase() : super(_openConnection());

  @override
  int get schemaVersion => 1;

  //Table
  Future<List<AppTable>> getListTable() async {
    final list = await (select(orderTable)).get();
    final numbers = list.map((e) => e.tableNumber).toSet().toList();
    List<AppTable> tables = [];
    for (var number in numbers) {
      final ordersData = list.where((e) => e.tableNumber == number).toList();
      final orders = ordersData
          .map(
            (item) => Order(
              productId: item.productId,
              count: item.count,
            ),
          )
          .toList();
      tables.add(AppTable(number: number, orders: orders));
    }
    return tables;
  }

  //Orders
  Future<List<Order>> getListOrder({required int tableNumber}) async {
    final list = await (select(orderTable)..where((t) => t.tableNumber.isIn([tableNumber]))).get();

    final data = list
        .map(
          (item) => Order(
            productId: item.productId,
            count: item.count,
          ),
        )
        .toList();
    return data;
  }

  Future<Order?> createOrder({
    required int productId,
    required int tableNumber,
  }) async {
    final companion = OrderTableCompanion(
      productId: drift.Value(productId),
      tableNumber: drift.Value(tableNumber),
      count: const drift.Value(1),
    );
    await into(orderTable).insert(companion);
    final list = await select(orderTable).get();
    return Order(
      productId: list.last.productId,
      count: list.last.count,
    );
  }

  Future<void> updateOrder({
    required int productId,
    required int tableNumber,
    required int count,
  }) async {
    (update(orderTable)
          ..where((tbl) => tbl.productId.isIn([productId]))
          ..where((tbl) => tbl.tableNumber.isIn([tableNumber])))
        .write(
      OrderTableCompanion(
        productId: drift.Value(productId),
        tableNumber: drift.Value(tableNumber),
        count: drift.Value(count),
      ),
    );
  }

  Future<void> deleteOrder({
    required int tableNumber,
    required int productId,
  }) async {
    await (delete(orderTable)
          ..where((t) => t.tableNumber.isIn([tableNumber]))
          ..where((t) => t.productId.isIn([productId])))
        .go();
  }
}
