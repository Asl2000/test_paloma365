// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// ignore_for_file: type=lint
class $OrderTableTable extends OrderTable
    with drift.TableInfo<$OrderTableTable, OrderTableData> {
  @override
  final drift.GeneratedDatabase attachedDatabase;
  final String? _alias;
  $OrderTableTable(this.attachedDatabase, [this._alias]);
  static const drift.VerificationMeta _idMeta =
      const drift.VerificationMeta('id');
  @override
  late final drift.GeneratedColumn<int> id = drift.GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const drift.VerificationMeta _productIdMeta =
      const drift.VerificationMeta('productId');
  @override
  late final drift.GeneratedColumn<int> productId = drift.GeneratedColumn<int>(
      'product_id', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const drift.VerificationMeta _tableNumberMeta =
      const drift.VerificationMeta('tableNumber');
  @override
  late final drift.GeneratedColumn<int> tableNumber =
      drift.GeneratedColumn<int>('table_number', aliasedName, false,
          type: DriftSqlType.int, requiredDuringInsert: true);
  static const drift.VerificationMeta _countMeta =
      const drift.VerificationMeta('count');
  @override
  late final drift.GeneratedColumn<int> count = drift.GeneratedColumn<int>(
      'count', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  @override
  List<drift.GeneratedColumn> get $columns =>
      [id, productId, tableNumber, count];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'order_table';
  @override
  drift.VerificationContext validateIntegrity(
      drift.Insertable<OrderTableData> instance,
      {bool isInserting = false}) {
    final context = drift.VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('product_id')) {
      context.handle(_productIdMeta,
          productId.isAcceptableOrUnknown(data['product_id']!, _productIdMeta));
    } else if (isInserting) {
      context.missing(_productIdMeta);
    }
    if (data.containsKey('table_number')) {
      context.handle(
          _tableNumberMeta,
          tableNumber.isAcceptableOrUnknown(
              data['table_number']!, _tableNumberMeta));
    } else if (isInserting) {
      context.missing(_tableNumberMeta);
    }
    if (data.containsKey('count')) {
      context.handle(
          _countMeta, count.isAcceptableOrUnknown(data['count']!, _countMeta));
    } else if (isInserting) {
      context.missing(_countMeta);
    }
    return context;
  }

  @override
  Set<drift.GeneratedColumn> get $primaryKey => {id};
  @override
  OrderTableData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return OrderTableData(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      productId: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}product_id'])!,
      tableNumber: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}table_number'])!,
      count: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}count'])!,
    );
  }

  @override
  $OrderTableTable createAlias(String alias) {
    return $OrderTableTable(attachedDatabase, alias);
  }
}

class OrderTableData extends drift.DataClass
    implements drift.Insertable<OrderTableData> {
  final int id;
  final int productId;
  final int tableNumber;
  final int count;
  const OrderTableData(
      {required this.id,
      required this.productId,
      required this.tableNumber,
      required this.count});
  @override
  Map<String, drift.Expression> toColumns(bool nullToAbsent) {
    final map = <String, drift.Expression>{};
    map['id'] = drift.Variable<int>(id);
    map['product_id'] = drift.Variable<int>(productId);
    map['table_number'] = drift.Variable<int>(tableNumber);
    map['count'] = drift.Variable<int>(count);
    return map;
  }

  OrderTableCompanion toCompanion(bool nullToAbsent) {
    return OrderTableCompanion(
      id: drift.Value(id),
      productId: drift.Value(productId),
      tableNumber: drift.Value(tableNumber),
      count: drift.Value(count),
    );
  }

  factory OrderTableData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= drift.driftRuntimeOptions.defaultSerializer;
    return OrderTableData(
      id: serializer.fromJson<int>(json['id']),
      productId: serializer.fromJson<int>(json['productId']),
      tableNumber: serializer.fromJson<int>(json['tableNumber']),
      count: serializer.fromJson<int>(json['count']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= drift.driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'productId': serializer.toJson<int>(productId),
      'tableNumber': serializer.toJson<int>(tableNumber),
      'count': serializer.toJson<int>(count),
    };
  }

  OrderTableData copyWith(
          {int? id, int? productId, int? tableNumber, int? count}) =>
      OrderTableData(
        id: id ?? this.id,
        productId: productId ?? this.productId,
        tableNumber: tableNumber ?? this.tableNumber,
        count: count ?? this.count,
      );
  @override
  String toString() {
    return (StringBuffer('OrderTableData(')
          ..write('id: $id, ')
          ..write('productId: $productId, ')
          ..write('tableNumber: $tableNumber, ')
          ..write('count: $count')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, productId, tableNumber, count);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is OrderTableData &&
          other.id == this.id &&
          other.productId == this.productId &&
          other.tableNumber == this.tableNumber &&
          other.count == this.count);
}

class OrderTableCompanion extends drift.UpdateCompanion<OrderTableData> {
  final drift.Value<int> id;
  final drift.Value<int> productId;
  final drift.Value<int> tableNumber;
  final drift.Value<int> count;
  const OrderTableCompanion({
    this.id = const drift.Value.absent(),
    this.productId = const drift.Value.absent(),
    this.tableNumber = const drift.Value.absent(),
    this.count = const drift.Value.absent(),
  });
  OrderTableCompanion.insert({
    this.id = const drift.Value.absent(),
    required int productId,
    required int tableNumber,
    required int count,
  })  : productId = drift.Value(productId),
        tableNumber = drift.Value(tableNumber),
        count = drift.Value(count);
  static drift.Insertable<OrderTableData> custom({
    drift.Expression<int>? id,
    drift.Expression<int>? productId,
    drift.Expression<int>? tableNumber,
    drift.Expression<int>? count,
  }) {
    return drift.RawValuesInsertable({
      if (id != null) 'id': id,
      if (productId != null) 'product_id': productId,
      if (tableNumber != null) 'table_number': tableNumber,
      if (count != null) 'count': count,
    });
  }

  OrderTableCompanion copyWith(
      {drift.Value<int>? id,
      drift.Value<int>? productId,
      drift.Value<int>? tableNumber,
      drift.Value<int>? count}) {
    return OrderTableCompanion(
      id: id ?? this.id,
      productId: productId ?? this.productId,
      tableNumber: tableNumber ?? this.tableNumber,
      count: count ?? this.count,
    );
  }

  @override
  Map<String, drift.Expression> toColumns(bool nullToAbsent) {
    final map = <String, drift.Expression>{};
    if (id.present) {
      map['id'] = drift.Variable<int>(id.value);
    }
    if (productId.present) {
      map['product_id'] = drift.Variable<int>(productId.value);
    }
    if (tableNumber.present) {
      map['table_number'] = drift.Variable<int>(tableNumber.value);
    }
    if (count.present) {
      map['count'] = drift.Variable<int>(count.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('OrderTableCompanion(')
          ..write('id: $id, ')
          ..write('productId: $productId, ')
          ..write('tableNumber: $tableNumber, ')
          ..write('count: $count')
          ..write(')'))
        .toString();
  }
}

abstract class _$AppDataBase extends drift.GeneratedDatabase {
  _$AppDataBase(QueryExecutor e) : super(e);
  _$AppDataBaseManager get managers => _$AppDataBaseManager(this);
  late final $OrderTableTable orderTable = $OrderTableTable(this);
  @override
  Iterable<drift.TableInfo<drift.Table, Object?>> get allTables =>
      allSchemaEntities.whereType<drift.TableInfo<drift.Table, Object?>>();
  @override
  List<drift.DatabaseSchemaEntity> get allSchemaEntities => [orderTable];
}

typedef $$OrderTableTableInsertCompanionBuilder = OrderTableCompanion Function({
  drift.Value<int> id,
  required int productId,
  required int tableNumber,
  required int count,
});
typedef $$OrderTableTableUpdateCompanionBuilder = OrderTableCompanion Function({
  drift.Value<int> id,
  drift.Value<int> productId,
  drift.Value<int> tableNumber,
  drift.Value<int> count,
});

class $$OrderTableTableTableManager extends drift.RootTableManager<
    _$AppDataBase,
    $OrderTableTable,
    OrderTableData,
    $$OrderTableTableFilterComposer,
    $$OrderTableTableOrderingComposer,
    $$OrderTableTableProcessedTableManager,
    $$OrderTableTableInsertCompanionBuilder,
    $$OrderTableTableUpdateCompanionBuilder> {
  $$OrderTableTableTableManager(_$AppDataBase db, $OrderTableTable table)
      : super(drift.TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$OrderTableTableFilterComposer(drift.ComposerState(db, table)),
          orderingComposer:
              $$OrderTableTableOrderingComposer(drift.ComposerState(db, table)),
          getChildManagerBuilder: (p) =>
              $$OrderTableTableProcessedTableManager(p),
          getUpdateCompanionBuilder: ({
            drift.Value<int> id = const drift.Value.absent(),
            drift.Value<int> productId = const drift.Value.absent(),
            drift.Value<int> tableNumber = const drift.Value.absent(),
            drift.Value<int> count = const drift.Value.absent(),
          }) =>
              OrderTableCompanion(
            id: id,
            productId: productId,
            tableNumber: tableNumber,
            count: count,
          ),
          getInsertCompanionBuilder: ({
            drift.Value<int> id = const drift.Value.absent(),
            required int productId,
            required int tableNumber,
            required int count,
          }) =>
              OrderTableCompanion.insert(
            id: id,
            productId: productId,
            tableNumber: tableNumber,
            count: count,
          ),
        ));
}

class $$OrderTableTableProcessedTableManager
    extends drift.ProcessedTableManager<
        _$AppDataBase,
        $OrderTableTable,
        OrderTableData,
        $$OrderTableTableFilterComposer,
        $$OrderTableTableOrderingComposer,
        $$OrderTableTableProcessedTableManager,
        $$OrderTableTableInsertCompanionBuilder,
        $$OrderTableTableUpdateCompanionBuilder> {
  $$OrderTableTableProcessedTableManager(super.$state);
}

class $$OrderTableTableFilterComposer
    extends drift.FilterComposer<_$AppDataBase, $OrderTableTable> {
  $$OrderTableTableFilterComposer(super.$state);
  drift.ColumnFilters<int> get id => $state.composableBuilder(
      column: $state.table.id,
      builder: (column, joinBuilders) =>
          drift.ColumnFilters(column, joinBuilders: joinBuilders));

  drift.ColumnFilters<int> get productId => $state.composableBuilder(
      column: $state.table.productId,
      builder: (column, joinBuilders) =>
          drift.ColumnFilters(column, joinBuilders: joinBuilders));

  drift.ColumnFilters<int> get tableNumber => $state.composableBuilder(
      column: $state.table.tableNumber,
      builder: (column, joinBuilders) =>
          drift.ColumnFilters(column, joinBuilders: joinBuilders));

  drift.ColumnFilters<int> get count => $state.composableBuilder(
      column: $state.table.count,
      builder: (column, joinBuilders) =>
          drift.ColumnFilters(column, joinBuilders: joinBuilders));
}

class $$OrderTableTableOrderingComposer
    extends drift.OrderingComposer<_$AppDataBase, $OrderTableTable> {
  $$OrderTableTableOrderingComposer(super.$state);
  drift.ColumnOrderings<int> get id => $state.composableBuilder(
      column: $state.table.id,
      builder: (column, joinBuilders) =>
          drift.ColumnOrderings(column, joinBuilders: joinBuilders));

  drift.ColumnOrderings<int> get productId => $state.composableBuilder(
      column: $state.table.productId,
      builder: (column, joinBuilders) =>
          drift.ColumnOrderings(column, joinBuilders: joinBuilders));

  drift.ColumnOrderings<int> get tableNumber => $state.composableBuilder(
      column: $state.table.tableNumber,
      builder: (column, joinBuilders) =>
          drift.ColumnOrderings(column, joinBuilders: joinBuilders));

  drift.ColumnOrderings<int> get count => $state.composableBuilder(
      column: $state.table.count,
      builder: (column, joinBuilders) =>
          drift.ColumnOrderings(column, joinBuilders: joinBuilders));
}

class _$AppDataBaseManager {
  final _$AppDataBase _db;
  _$AppDataBaseManager(this._db);
  $$OrderTableTableTableManager get orderTable =>
      $$OrderTableTableTableManager(_db, _db.orderTable);
}
