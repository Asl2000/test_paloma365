import 'package:test_paloma365/data/models/order.dart';
import 'package:test_paloma365/data/database/database.dart';
import 'package:test_paloma365/data/models/answer.dart';
import 'package:test_paloma365/domain/repositories/order_repository.dart';

class ApiOrderRepository implements OrderRepository {
  final AppDataBase dataBase;

  ApiOrderRepository({required this.dataBase});

  @override
  Future<Answer<List<Order>>> getListOrder({required int tableNumber}) async {
    final orders = await dataBase.getListOrder(tableNumber: tableNumber);
    return Answer(data: orders);
  }

  @override
  Future<void> updateOrder({
    required int productId,
    required int tableNumber,
    required int count,
  }) {
    return dataBase.updateOrder(
      tableNumber: tableNumber,
      productId: productId,
      count: count,
    );
  }

  @override
  Future<Order?> createOrder({
    required int productId,
    required int tableNumber,
  }) {
    return dataBase.createOrder(
      tableNumber: tableNumber,
      productId: productId,
    );
  }

  @override
  Future<void> deleteOrder({
    required int tableNumber,
    required int productId,
  }) {
    return dataBase.deleteOrder(
      tableNumber: tableNumber,
      productId: productId,
    );
  }
}
