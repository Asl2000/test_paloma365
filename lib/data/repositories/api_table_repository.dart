import 'package:test_paloma365/data/database/database.dart';
import 'package:test_paloma365/data/models/answer.dart';
import 'package:test_paloma365/data/models/table.dart';
import 'package:test_paloma365/domain/repositories/table_repository.dart';

class ApiTableRepository implements TableRepository {
  final AppDataBase dataBase;

  ApiTableRepository({required this.dataBase});

  @override
  Future<Answer<List<AppTable>>> getListTable() async {
    final tables = await dataBase.getListTable();
    return Answer(data: tables);
  }
}
