import 'package:test_paloma365/data/data/products.dart';
import 'package:test_paloma365/data/models/answer.dart';
import 'package:test_paloma365/data/models/product.dart';
import 'package:test_paloma365/domain/repositories/product_repository.dart';

class ApiProductRepository implements ProductRepository {
  @override
  Future<Answer<List<Product>>> getListProduct() async {
    return Answer(data: productsData);
  }
}
