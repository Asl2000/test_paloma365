import 'package:test_paloma365/data/models/product.dart';
import 'package:test_paloma365/domain/enums/product_type.dart';

final productsData = [
  //Burgers
  Product(
    name: 'Бургер куриный с картофелем «фри»',
    id: 1,
    imagePath: '/menu/burgers/burger_kurinnaya.jpg',
    price: 10.5,
    type: ProductType.burgers,
  ),
  Product(
    name: 'Бургер Трюфельный с картофелем «фри»',
    id: 2,
    imagePath: '/menu/burgers/burger_telaytina.jpg',
    price: 15.5,
    type: ProductType.burgers,
  ),
  //Desserts
  Product(
    name: 'Медовик',
    id: 3,
    imagePath: '/menu/desserts/medovik.jpg',
    price: 5.25,
    type: ProductType.desserts,
  ),
  Product(
    name: 'Мусс шокаладный с вишней',
    id: 4,
    imagePath: '/menu/desserts/mus_shokoladny.jpg',
    price: 5.25,
    type: ProductType.desserts,
  ),
  Product(
    name: 'Сорбеты',
    id: 5,
    imagePath: '/menu/desserts/sorbet.jpg',
    price: 2.5,
    type: ProductType.desserts,
  ),
  Product(
    name: 'Тортик Манго-Маракуйя',
    id: 6,
    imagePath: '/menu/desserts/tort_mango.jpg',
    price: 5.5,
    type: ProductType.desserts,
  ),
  //Soups
  Product(
    name: 'Борщ со сметаной',
    id: 7,
    imagePath: '/menu/soups/borsh.jpg',
    price: 5.5,
    type: ProductType.soup,
  ),
  Product(
    name: 'Суп «Том Ям»',
    id: 8,
    imagePath: '/menu/soups/tom_yam.jpg',
    price: 10.0,
    type: ProductType.soup,
  ),
  //Sous
  Product(
    name: 'Аджика домашняя из томатов с чесноком',
    id: 9,
    imagePath: '/menu/sous/adzhika.jpg',
    price: 1.75,
    type: ProductType.sous,
  ),
  Product(
    name: 'Соус гранатовый Наршараб',
    id: 10,
    imagePath: '/menu/sous/narsharab.jpg',
    price: 1.75,
    type: ProductType.sous,
  ),
  Product(
    name: 'Оливковое масло',
    id: 11,
    imagePath: '/menu/sous/olive_oil.jpg',
    price: 2.0,
    type: ProductType.sous,
  ),
  Product(
    name: 'Соус Тар Тар',
    id: 12,
    imagePath: '/menu/sous/tartar.jpg',
    price: 1.75,
    type: ProductType.sous,
  ),
  //Hot
  Product(
    name: 'Котлеты куриные с картофельным пюрер',
    id: 13,
    imagePath: '/menu/hot/kotlety_kurinie.jpg',
    price: 8,
    type: ProductType.hot,
  ),
  Product(
    name: 'Креветки Темпура',
    id: 14,
    imagePath: '/menu/hot/krevetki_tempura.jpg',
    price: 7.75,
    type: ProductType.hot,
  ),
  Product(
    name: 'Морепродукты на теппане',
    id: 15,
    imagePath: '/menu/hot/moreproducty_tepan.jpg',
    price: 12.35,
    type: ProductType.hot,
  ),
  Product(
    name: 'Шашлык из свиной шейки',
    id: 16,
    imagePath: '/menu/hot/shahlyk_svinina.jpg',
    price: 8.0,
    type: ProductType.hot,
  ),
  Product(
    name: 'Сибас на гриле',
    id: 17,
    imagePath: '/menu/hot/sibas_grill.jpg',
    price: 15.5,
    type: ProductType.hot,
  ),
];
