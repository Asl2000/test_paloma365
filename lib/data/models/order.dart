class Order {
  final int productId;
  late int count;

  Order({
    required this.productId,
    required this.count,
  });
}
