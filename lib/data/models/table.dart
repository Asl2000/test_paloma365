import 'package:test_paloma365/data/models/order.dart';

class AppTable {
  final int number;
  late List<Order> orders;

  AppTable({
    required this.number,
    required this.orders,
  });
}
