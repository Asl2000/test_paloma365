import 'package:test_paloma365/domain/enums/product_type.dart';

class Product {
  final int id;
  final String name;
  final String imagePath;
  final double price;
  final ProductType type;

  Product({
    required this.id,
    required this.name,
    required this.imagePath,
    required this.price,
    required this.type,
  });

  String get image => 'assets$imagePath';
}
