import 'package:flutter/material.dart';
import 'package:test_paloma365/ui/screens/table/table_detail_screen.dart';
import 'package:test_paloma365/ui/screens/table/tables_screen.dart';

class AppRouter {
  static const String tables = '/tables';
  static const String tableDetail = '/table-detail';

  static MaterialPageRoute onGenerateRouters(RouteSettings settings) {
    final args = settings.arguments as AppRouterArguments?;

    final routes = <String, WidgetBuilder>{
      AppRouter.tables: (BuildContext context) => const TablesScreen(),
      AppRouter.tableDetail: (BuildContext context) => TableDetailScreen(number: args!.number!),
    };

    WidgetBuilder? builder = routes[settings.name!];
    return MaterialPageRoute(builder: (ctx) => builder!(ctx));
  }
}

class AppRouterArguments {
  final int? number;

  AppRouterArguments({this.number});
}
