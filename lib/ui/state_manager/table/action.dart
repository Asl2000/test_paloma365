import 'package:test_paloma365/data/models/table.dart';

abstract class TableListAction {}

class LoadTableListAction extends TableListAction {}

class ShowTableListAction extends TableListAction {
  final List<AppTable> tables;

  ShowTableListAction({required this.tables});
}

class ErrorTableListAction extends TableListAction {
  final String message;

  ErrorTableListAction({required this.message});
}
