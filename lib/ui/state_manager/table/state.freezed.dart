// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$TableListState {
  bool get isLoading => throw _privateConstructorUsedError;
  bool get isError => throw _privateConstructorUsedError;
  String get errorMessage => throw _privateConstructorUsedError;
  List<AppTable> get tables => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $TableListStateCopyWith<TableListState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TableListStateCopyWith<$Res> {
  factory $TableListStateCopyWith(
          TableListState value, $Res Function(TableListState) then) =
      _$TableListStateCopyWithImpl<$Res, TableListState>;
  @useResult
  $Res call(
      {bool isLoading,
      bool isError,
      String errorMessage,
      List<AppTable> tables});
}

/// @nodoc
class _$TableListStateCopyWithImpl<$Res, $Val extends TableListState>
    implements $TableListStateCopyWith<$Res> {
  _$TableListStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLoading = null,
    Object? isError = null,
    Object? errorMessage = null,
    Object? tables = null,
  }) {
    return _then(_value.copyWith(
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isError: null == isError
          ? _value.isError
          : isError // ignore: cast_nullable_to_non_nullable
              as bool,
      errorMessage: null == errorMessage
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String,
      tables: null == tables
          ? _value.tables
          : tables // ignore: cast_nullable_to_non_nullable
              as List<AppTable>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$TableListStateImplCopyWith<$Res>
    implements $TableListStateCopyWith<$Res> {
  factory _$$TableListStateImplCopyWith(_$TableListStateImpl value,
          $Res Function(_$TableListStateImpl) then) =
      __$$TableListStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {bool isLoading,
      bool isError,
      String errorMessage,
      List<AppTable> tables});
}

/// @nodoc
class __$$TableListStateImplCopyWithImpl<$Res>
    extends _$TableListStateCopyWithImpl<$Res, _$TableListStateImpl>
    implements _$$TableListStateImplCopyWith<$Res> {
  __$$TableListStateImplCopyWithImpl(
      _$TableListStateImpl _value, $Res Function(_$TableListStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLoading = null,
    Object? isError = null,
    Object? errorMessage = null,
    Object? tables = null,
  }) {
    return _then(_$TableListStateImpl(
      null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      null == isError
          ? _value.isError
          : isError // ignore: cast_nullable_to_non_nullable
              as bool,
      null == errorMessage
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String,
      null == tables
          ? _value._tables
          : tables // ignore: cast_nullable_to_non_nullable
              as List<AppTable>,
    ));
  }
}

/// @nodoc

class _$TableListStateImpl implements _TableListState {
  _$TableListStateImpl(
      [this.isLoading = false,
      this.isError = false,
      this.errorMessage = '',
      final List<AppTable> tables = const []])
      : _tables = tables;

  @override
  @JsonKey()
  final bool isLoading;
  @override
  @JsonKey()
  final bool isError;
  @override
  @JsonKey()
  final String errorMessage;
  final List<AppTable> _tables;
  @override
  @JsonKey()
  List<AppTable> get tables {
    if (_tables is EqualUnmodifiableListView) return _tables;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_tables);
  }

  @override
  String toString() {
    return 'TableListState(isLoading: $isLoading, isError: $isError, errorMessage: $errorMessage, tables: $tables)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TableListStateImpl &&
            (identical(other.isLoading, isLoading) ||
                other.isLoading == isLoading) &&
            (identical(other.isError, isError) || other.isError == isError) &&
            (identical(other.errorMessage, errorMessage) ||
                other.errorMessage == errorMessage) &&
            const DeepCollectionEquality().equals(other._tables, _tables));
  }

  @override
  int get hashCode => Object.hash(runtimeType, isLoading, isError, errorMessage,
      const DeepCollectionEquality().hash(_tables));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$TableListStateImplCopyWith<_$TableListStateImpl> get copyWith =>
      __$$TableListStateImplCopyWithImpl<_$TableListStateImpl>(
          this, _$identity);
}

abstract class _TableListState implements TableListState {
  factory _TableListState(
      [final bool isLoading,
      final bool isError,
      final String errorMessage,
      final List<AppTable> tables]) = _$TableListStateImpl;

  @override
  bool get isLoading;
  @override
  bool get isError;
  @override
  String get errorMessage;
  @override
  List<AppTable> get tables;
  @override
  @JsonKey(ignore: true)
  _$$TableListStateImplCopyWith<_$TableListStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
