import 'package:redux/redux.dart';
import 'package:test_paloma365/ui/state_manager/table/action.dart';
import 'package:test_paloma365/ui/state_manager/table/state.dart';

final tableListReducer = combineReducers<TableListState>([
  TypedReducer<TableListState, LoadTableListAction>(_loadTableList).call,
  TypedReducer<TableListState, ShowTableListAction>(_showTableList).call,
  TypedReducer<TableListState, ErrorTableListAction>(_errorTableList).call,
]);

// Tables list

TableListState _loadTableList(
  TableListState state,
  LoadTableListAction action,
) =>
    state.copyWith(
      isLoading: true,
      isError: false,
    );

TableListState _showTableList(
  TableListState state,
  ShowTableListAction action,
) =>
    state.copyWith(
      isLoading: false,
      isError: false,
      tables: action.tables,
    );

TableListState _errorTableList(
  TableListState state,
  ErrorTableListAction action,
) =>
    state.copyWith(
      isLoading: false,
      isError: true,
      errorMessage: action.message,
    );