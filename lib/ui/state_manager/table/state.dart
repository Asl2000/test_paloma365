import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:test_paloma365/data/models/table.dart';

part 'state.freezed.dart';

@freezed
class TableListState with _$TableListState {
  factory TableListState([
    @Default(false) bool isLoading,
    @Default(false) bool isError,
    @Default('') String errorMessage,
    @Default([]) List<AppTable> tables,
  ]) = _TableListState;
}
