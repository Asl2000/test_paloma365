import 'package:redux/redux.dart';
import 'package:test_paloma365/domain/repositories/table_repository.dart';
import 'package:test_paloma365/ui/state_manager/store.dart';
import 'package:test_paloma365/ui/state_manager/table/action.dart';

class TableMiddleware implements MiddlewareClass<AppState> {
  final TableRepository tableRepository;

  TableMiddleware({required this.tableRepository});

  @override
  call(store, action, next) {
    if (action is LoadTableListAction) {
      Future(() async {
        final answer = await tableRepository.getListTable();
        if (answer.data != null) {
          store.dispatch(
            ShowTableListAction(tables: answer.data!),
          );
        } else {
          store.dispatch(ErrorTableListAction(message: answer.error!));
        }
      });
    }

    next(action);
  }
}
