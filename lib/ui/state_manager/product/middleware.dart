import 'package:redux/redux.dart';
import 'package:test_paloma365/domain/repositories/product_repository.dart';
import 'package:test_paloma365/ui/state_manager/product/action.dart';
import 'package:test_paloma365/ui/state_manager/store.dart';

class ProductMiddleware implements MiddlewareClass<AppState> {
  final ProductRepository productRepository;

  ProductMiddleware({required this.productRepository});

  @override
  call(store, action, next) {
    // List products
    if (action is LoadProductListAction) {
      Future(() async {
        final answer = await productRepository.getListProduct();
        if (answer.data != null) {
          store.dispatch(ShowProductListAction(products: answer.data!));
        } else {
          store.dispatch(ErrorProductListAction(message: answer.error!));
        }
      });
    }

    next(action);
  }
}
