import 'package:test_paloma365/data/models/product.dart';

abstract class ProductListAction {}

class LoadProductListAction extends ProductListAction {}

class ShowProductListAction extends ProductListAction {
  final List<Product> products;

  ShowProductListAction({required this.products});
}

class ErrorProductListAction extends ProductListAction {
  final String message;

  ErrorProductListAction({required this.message});
}
