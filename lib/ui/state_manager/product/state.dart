import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:test_paloma365/data/models/product.dart';

part 'state.freezed.dart';

@freezed
class ProductListState with _$ProductListState {
  factory ProductListState([
    @Default(false) bool isLoading,
    @Default(false) bool isError,
    @Default('') String errorMessage,
    @Default([]) List<Product> products,
  ]) = _ProductListState;
}
