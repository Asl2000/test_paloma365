import 'package:redux/redux.dart';
import 'package:test_paloma365/ui/state_manager/product/action.dart';
import 'package:test_paloma365/ui/state_manager/product/state.dart';

final productListReducer = combineReducers<ProductListState>([
  TypedReducer<ProductListState, LoadProductListAction>(_loadProductList).call,
  TypedReducer<ProductListState, ShowProductListAction>(_showProductList).call,
  TypedReducer<ProductListState, ErrorProductListAction>(_errorProductList).call,
]);

//List Products

ProductListState _loadProductList(
  ProductListState state,
  LoadProductListAction action,
) =>
    state.copyWith(
      isLoading: true,
      isError: false,
    );

ProductListState _showProductList(
  ProductListState state,
  ShowProductListAction action,
) =>
    state.copyWith(
      isLoading: false,
      isError: false,
      products: action.products,
    );

ProductListState _errorProductList(
  ProductListState state,
  ErrorProductListAction action,
) =>
    state.copyWith(
      isLoading: false,
      isError: true,
      errorMessage: action.message,
    );
