import 'package:test_paloma365/ui/state_manager/order/action.dart';
import 'package:test_paloma365/ui/state_manager/order/reducer.dart';
import 'package:test_paloma365/ui/state_manager/product/action.dart';
import 'package:test_paloma365/ui/state_manager/product/reducer.dart';
import 'package:test_paloma365/ui/state_manager/store.dart';
import 'package:test_paloma365/ui/state_manager/table/action.dart';
import 'package:test_paloma365/ui/state_manager/table/reducer.dart';

AppState appReducer(AppState state, dynamic action) {
  if (action is OrderListAction) {
    final nextState = orderListReducer(state.orderListState, action);
    return state.copyWith(orderListState: nextState);
  } else if (action is TableListAction) {
    final nextState = tableListReducer(state.tableListState, action);
    return state.copyWith(tableListState: nextState);
  } else if (action is ProductListAction) {
    final nextState = productListReducer(state.productListState, action);
    return state.copyWith(productListState: nextState);
  }
  return state;
}
