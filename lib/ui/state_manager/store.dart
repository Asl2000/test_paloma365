import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:test_paloma365/ui/state_manager/order/state.dart';
import 'package:test_paloma365/ui/state_manager/product/state.dart';
import 'package:test_paloma365/ui/state_manager/table/state.dart';

part 'store.freezed.dart';

@freezed
class AppState with _$AppState {
  const factory AppState({
    required ProductListState productListState,
    required OrderListState orderListState,
    required TableListState tableListState,
  }) = _AppState;
}
