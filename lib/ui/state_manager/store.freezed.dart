// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'store.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$AppState {
  ProductListState get productListState => throw _privateConstructorUsedError;
  OrderListState get orderListState => throw _privateConstructorUsedError;
  TableListState get tableListState => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AppStateCopyWith<AppState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppStateCopyWith<$Res> {
  factory $AppStateCopyWith(AppState value, $Res Function(AppState) then) =
      _$AppStateCopyWithImpl<$Res, AppState>;
  @useResult
  $Res call(
      {ProductListState productListState,
      OrderListState orderListState,
      TableListState tableListState});

  $ProductListStateCopyWith<$Res> get productListState;
  $OrderListStateCopyWith<$Res> get orderListState;
  $TableListStateCopyWith<$Res> get tableListState;
}

/// @nodoc
class _$AppStateCopyWithImpl<$Res, $Val extends AppState>
    implements $AppStateCopyWith<$Res> {
  _$AppStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? productListState = null,
    Object? orderListState = null,
    Object? tableListState = null,
  }) {
    return _then(_value.copyWith(
      productListState: null == productListState
          ? _value.productListState
          : productListState // ignore: cast_nullable_to_non_nullable
              as ProductListState,
      orderListState: null == orderListState
          ? _value.orderListState
          : orderListState // ignore: cast_nullable_to_non_nullable
              as OrderListState,
      tableListState: null == tableListState
          ? _value.tableListState
          : tableListState // ignore: cast_nullable_to_non_nullable
              as TableListState,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $ProductListStateCopyWith<$Res> get productListState {
    return $ProductListStateCopyWith<$Res>(_value.productListState, (value) {
      return _then(_value.copyWith(productListState: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $OrderListStateCopyWith<$Res> get orderListState {
    return $OrderListStateCopyWith<$Res>(_value.orderListState, (value) {
      return _then(_value.copyWith(orderListState: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $TableListStateCopyWith<$Res> get tableListState {
    return $TableListStateCopyWith<$Res>(_value.tableListState, (value) {
      return _then(_value.copyWith(tableListState: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$AppStateImplCopyWith<$Res>
    implements $AppStateCopyWith<$Res> {
  factory _$$AppStateImplCopyWith(
          _$AppStateImpl value, $Res Function(_$AppStateImpl) then) =
      __$$AppStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {ProductListState productListState,
      OrderListState orderListState,
      TableListState tableListState});

  @override
  $ProductListStateCopyWith<$Res> get productListState;
  @override
  $OrderListStateCopyWith<$Res> get orderListState;
  @override
  $TableListStateCopyWith<$Res> get tableListState;
}

/// @nodoc
class __$$AppStateImplCopyWithImpl<$Res>
    extends _$AppStateCopyWithImpl<$Res, _$AppStateImpl>
    implements _$$AppStateImplCopyWith<$Res> {
  __$$AppStateImplCopyWithImpl(
      _$AppStateImpl _value, $Res Function(_$AppStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? productListState = null,
    Object? orderListState = null,
    Object? tableListState = null,
  }) {
    return _then(_$AppStateImpl(
      productListState: null == productListState
          ? _value.productListState
          : productListState // ignore: cast_nullable_to_non_nullable
              as ProductListState,
      orderListState: null == orderListState
          ? _value.orderListState
          : orderListState // ignore: cast_nullable_to_non_nullable
              as OrderListState,
      tableListState: null == tableListState
          ? _value.tableListState
          : tableListState // ignore: cast_nullable_to_non_nullable
              as TableListState,
    ));
  }
}

/// @nodoc

class _$AppStateImpl implements _AppState {
  const _$AppStateImpl(
      {required this.productListState,
      required this.orderListState,
      required this.tableListState});

  @override
  final ProductListState productListState;
  @override
  final OrderListState orderListState;
  @override
  final TableListState tableListState;

  @override
  String toString() {
    return 'AppState(productListState: $productListState, orderListState: $orderListState, tableListState: $tableListState)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AppStateImpl &&
            (identical(other.productListState, productListState) ||
                other.productListState == productListState) &&
            (identical(other.orderListState, orderListState) ||
                other.orderListState == orderListState) &&
            (identical(other.tableListState, tableListState) ||
                other.tableListState == tableListState));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, productListState, orderListState, tableListState);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AppStateImplCopyWith<_$AppStateImpl> get copyWith =>
      __$$AppStateImplCopyWithImpl<_$AppStateImpl>(this, _$identity);
}

abstract class _AppState implements AppState {
  const factory _AppState(
      {required final ProductListState productListState,
      required final OrderListState orderListState,
      required final TableListState tableListState}) = _$AppStateImpl;

  @override
  ProductListState get productListState;
  @override
  OrderListState get orderListState;
  @override
  TableListState get tableListState;
  @override
  @JsonKey(ignore: true)
  _$$AppStateImplCopyWith<_$AppStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
