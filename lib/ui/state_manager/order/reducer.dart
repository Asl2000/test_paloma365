import 'package:redux/redux.dart';
import 'package:test_paloma365/ui/state_manager/order/action.dart';
import 'package:test_paloma365/ui/state_manager/order/state.dart';

final orderListReducer = combineReducers<OrderListState>([
  TypedReducer<OrderListState, LoadOrderListAction>(_loadOrderList).call,
  TypedReducer<OrderListState, ShowOrderListAction>(_showOrderList).call,
  TypedReducer<OrderListState, ErrorOrderListAction>(_errorOrderList).call,
]);

// Orders list

OrderListState _loadOrderList(
  OrderListState state,
  LoadOrderListAction action,
) =>
    state.copyWith(
      isLoading: true,
      isError: false,
    );

OrderListState _showOrderList(
  OrderListState state,
  ShowOrderListAction action,
) =>
    state.copyWith(
      isLoading: false,
      isError: false,
      numberTable: action.nubmer,
      orders: action.orders,
    );

OrderListState _errorOrderList(
  OrderListState state,
  ErrorOrderListAction action,
) =>
    state.copyWith(
      isLoading: false,
      isError: true,
      errorMessage: action.message,
    );
