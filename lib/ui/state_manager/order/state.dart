import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:test_paloma365/data/models/order.dart';

part 'state.freezed.dart';

@freezed
class OrderListState with _$OrderListState {
  factory OrderListState([
    @Default(false) bool isLoading,
    @Default(false) bool isError,
    @Default('') String errorMessage,
    @Default(-1) int numberTable,
    @Default([]) List<Order> orders,
  ]) = _OrderListState;
}
