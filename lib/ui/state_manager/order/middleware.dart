import 'package:redux/redux.dart';
import 'package:test_paloma365/domain/repositories/order_repository.dart';
import 'package:test_paloma365/ui/state_manager/order/action.dart';
import 'package:test_paloma365/ui/state_manager/store.dart';

class OrderMiddleware implements MiddlewareClass<AppState> {
  final OrderRepository orderRepository;

  OrderMiddleware({
    required this.orderRepository,
  });

  @override
  call(store, action, next) {
    // Table
    if (action is LoadOrderListAction) {
      Future(() async {
        final answer = await orderRepository.getListOrder(tableNumber: action.number);
        if (answer.data != null) {
          store.dispatch(
            ShowOrderListAction(
              orders: answer.data!,
              nubmer: action.number,
            ),
          );
        } else {
          store.dispatch(ErrorOrderListAction(message: answer.error!));
        }
      });
    }

    if (action is AddOrderListAction) {
      final orders = store.state.orderListState.orders.toList();
      orders.add(action.order);
      store.dispatch(
        ShowOrderListAction(
          nubmer: store.state.orderListState.numberTable,
          orders: orders,
        ),
      );
    }

    if (action is UpdateOrderListAction) {
      final orders = store.state.orderListState.orders.toList();
      final index = orders.indexWhere((e) => e.productId == action.order.productId);
      orders[index] = action.order;
      store.dispatch(
        ShowOrderListAction(
          nubmer: store.state.orderListState.numberTable,
          orders: orders,
        ),
      );
    }

    next(action);
  }
}
