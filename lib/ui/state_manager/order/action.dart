import 'package:test_paloma365/data/models/order.dart';

abstract class OrderListAction {}

class LoadOrderListAction extends OrderListAction {
  final int number;

  LoadOrderListAction({required this.number});
}

class ShowOrderListAction extends OrderListAction {
  final List<Order> orders;
  final int nubmer;

  ShowOrderListAction({
    required this.orders,
    required this.nubmer,
  });
}

class AddOrderListAction extends OrderListAction {
  final Order order;

  AddOrderListAction({required this.order});
}

class UpdateOrderListAction extends OrderListAction {
  final Order order;

  UpdateOrderListAction({required this.order});
}

class ErrorOrderListAction extends OrderListAction {
  final String message;

  ErrorOrderListAction({required this.message});
}
