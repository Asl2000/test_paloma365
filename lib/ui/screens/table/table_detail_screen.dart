import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:redux/redux.dart';
import 'package:test_paloma365/domain/enums/product_type.dart';
import 'package:test_paloma365/ui/screens/table/widgets/product_card.dart';
import 'package:test_paloma365/ui/screens/table/widgets/product_type_card.dart';
import 'package:test_paloma365/ui/state_manager/order/action.dart';
import 'package:test_paloma365/ui/state_manager/order/state.dart';
import 'package:test_paloma365/ui/state_manager/product/action.dart';
import 'package:test_paloma365/ui/state_manager/product/state.dart';
import 'package:test_paloma365/ui/state_manager/store.dart';

class TableDetailScreen extends StatefulWidget {
  final int number;

  const TableDetailScreen({
    super.key,
    required this.number,
  });

  @override
  State<TableDetailScreen> createState() => _TableDetailScreenState();
}

class _TableDetailScreenState extends State<TableDetailScreen> {
  late ProductType? selectedType;
  late Store<AppState> _store;

  @override
  void initState() {
    super.initState();
    selectedType = null;
    _store = StoreProvider.of<AppState>(context, listen: false);
    if (_store.state.productListState.products.isEmpty) {
      _store.dispatch(LoadProductListAction());
    }
    if (_store.state.orderListState.numberTable != widget.number) {
      _store.dispatch(LoadOrderListAction(number: widget.number));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'Стол №${widget.number}',
          style: GoogleFonts.montserrat(
            fontSize: 20,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8),
        child: Column(
          children: [
            SizedBox(
              height: 30,
              child: ListView(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                children: [
                  const SizedBox(width: 16),
                  for (var type in ProductType.values) ...[
                    GestureDetector(
                      onTap: () {
                        if (selectedType == type) {
                          selectedType = null;
                        } else {
                          selectedType = type;
                        }
                        setState(() {});
                      },
                      child: ProductTypeCard(
                        active: selectedType == type,
                        productType: type,
                      ),
                    ),
                    const SizedBox(width: 8),
                  ],
                  const SizedBox(width: 8),
                ],
              ),
            ),
            Expanded(
              child: StoreConnector<AppState, OrderListState>(
                converter: (tableStore) => tableStore.state.orderListState,
                builder: (BuildContext context, OrderListState tableStore) {
                  if (tableStore.isLoading) {
                    return const Center(
                      child: CircularProgressIndicator(color: Colors.red),
                    );
                  }
                  if (tableStore.isError) {
                    return Center(
                      child: Text(
                        tableStore.errorMessage,
                        textAlign: TextAlign.center,
                      ),
                    );
                  }
                  return StoreConnector<AppState, ProductListState>(
                    converter: (store) => store.state.productListState,
                    builder: (BuildContext context, ProductListState state) {
                      if (state.isLoading) {
                        return const Center(
                          child: CircularProgressIndicator(color: Colors.red),
                        );
                      }
                      if (state.isError) {
                        return Center(
                          child: Text(
                            state.errorMessage,
                            textAlign: TextAlign.center,
                          ),
                        );
                      }
                      var products = state.products;
                      if (selectedType != null) {
                        products = products.where((e) => e.type == selectedType).toList();
                      }
                      return ListView.separated(
                        itemCount: products.length,
                        padding: const EdgeInsets.symmetric(
                          horizontal: 16,
                          vertical: 12,
                        ),
                        separatorBuilder: (_, __) => const SizedBox(height: 12),
                        itemBuilder: (_, index) {
                          final product = products[index];
                          final order =
                              tableStore.orders.where((e) => e.productId == product.id).toList();
                          return ProductCard(
                            product: product,
                            order: order.isEmpty ? null : order.first,
                          );
                        },
                      );
                    },
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
