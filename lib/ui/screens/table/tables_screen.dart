import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:redux/redux.dart';
import 'package:test_paloma365/domain/di/get_it_service.dart';
import 'package:test_paloma365/ui/state_manager/store.dart';
import 'package:test_paloma365/ui/state_manager/table/action.dart';
import 'package:test_paloma365/ui/state_manager/table/state.dart';
import 'package:test_paloma365/ui/widgets/app_card.dart';

class TablesScreen extends StatefulWidget {
  const TablesScreen({super.key});

  @override
  State<TablesScreen> createState() => _TablesScreenState();
}

class _TablesScreenState extends State<TablesScreen> {
  late Store<AppState> _store;

  @override
  void initState() {
    super.initState();
    _store = StoreProvider.of<AppState>(context, listen: false);
    _store.dispatch(LoadTableListAction());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'Выбер стола',
          style: GoogleFonts.montserrat(
            fontSize: 20,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8),
        child: StoreConnector<AppState, TableListState>(
          converter: (store) => store.state.tableListState,
          builder: (BuildContext context, TableListState state) {
            if (state.isLoading) {
              return const Center(
                child: CircularProgressIndicator(color: Colors.red),
              );
            }
            if (state.isError) {
              return Center(
                child: Text(
                  state.errorMessage,
                  textAlign: TextAlign.center,
                ),
              );
            }
            return GridView.builder(
              itemCount: 16,
              padding: const EdgeInsets.symmetric(horizontal: 16),
              itemBuilder: (_, index) {
                final number = index + 1;
                final table = state.tables.where((e) => e.number == number).toList();
                return Stack(
                  alignment: Alignment.topRight,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 12, right: 5),
                      child: AppCard(
                        onTap: () => getItService.navigatorService.onTableDetail(
                          number: index + 1,
                        ),
                        child: Center(
                          child: Text(
                            'Стол №$number',
                            style: GoogleFonts.montserrat(
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ),
                    ),
                    if (table.isNotEmpty)
                      if (table.first.orders.isNotEmpty)
                        Container(
                          padding: const EdgeInsets.all(5),
                          decoration: const BoxDecoration(
                            color: Colors.red,
                            shape: BoxShape.circle,
                          ),
                          child: Text(
                            table.first.orders.length.toString(),
                            style: GoogleFonts.montserrat(
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              color: Colors.white,
                            ),
                          ),
                        ),
                  ],
                );
              },
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 2,
                crossAxisSpacing: 5,
                mainAxisSpacing: 0,
              ),
            );
          },
        ),
      ),
    );
  }
}
