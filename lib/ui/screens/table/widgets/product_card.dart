import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:test_paloma365/data/models/order.dart';
import 'package:test_paloma365/data/models/product.dart';
import 'package:test_paloma365/domain/di/get_it_service.dart';
import 'package:test_paloma365/ui/resurses/icons.dart';
import 'package:test_paloma365/ui/widgets/app_card.dart';

class ProductCard extends StatelessWidget {
  final Product product;
  final Order? order;

  const ProductCard({
    super.key,
    required this.product,
    this.order,
  });

  @override
  Widget build(BuildContext context) {
    return AppCard(
      borderColor: (order?.count ?? 0) == 0 ? null : Colors.red,
      child: Row(
        children: [
          Image.asset(
            product.image,
            width: 60,
            height: 50,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  product.name,
                  style: GoogleFonts.montserrat(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                const SizedBox(height: 4),
                Text(
                  '\$${product.price}',
                  style: GoogleFonts.montserrat(
                    fontSize: 14,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(width: 16),
          if ((order?.count ?? 0) == 0)
            GestureDetector(
              onTap: () {
                getItService.orderController.addProduct(
                  productId: product.id,
                  order: order,
                );
              },
              child: Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 8,
                  horizontal: 16,
                ),
                decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Text(
                  'Добавить',
                  style: GoogleFonts.montserrat(
                    fontSize: 14,
                    fontWeight: FontWeight.w700,
                    color: Colors.white,
                  ),
                ),
              ),
            )
          else ...[
            GestureDetector(
              onTap: () => getItService.orderController.removeProduct(
                order: order!,
              ),
              child: GestureDetector(
                child: SvgPicture.asset(
                  AppIcons.minus,
                  width: 24,
                  height: 24,
                ),
              ),
            ),
            const SizedBox(width: 8),
            Text(
              order!.count.toString(),
              style: GoogleFonts.montserrat(
                fontSize: 20,
                fontWeight: FontWeight.w500,
              ),
            ),
            const SizedBox(width: 8),
            GestureDetector(
              onTap: () => getItService.orderController.addProduct(
                productId: product.id,
                order: order,
              ),
              child: SvgPicture.asset(
                AppIcons.plus,
                width: 24,
                height: 24,
              ),
            ),
          ],
        ],
      ),
    );
  }
}
