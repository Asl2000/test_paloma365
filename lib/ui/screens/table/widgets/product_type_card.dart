import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:test_paloma365/domain/deserializer/product_type_deserializer.dart';
import 'package:test_paloma365/domain/deserializer/product_type_emoji_deserializer.dart';
import 'package:test_paloma365/domain/enums/product_type.dart';

class ProductTypeCard extends StatelessWidget {
  final ProductType productType;
  final bool active;

  const ProductTypeCard({
    super.key,
    required this.productType,
    required this.active,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        border: Border.all(
          color: active ? Colors.transparent : Colors.grey,
          width: 1.5,
        ),
        color: active ? Colors.red : Colors.transparent,
      ),
      child: Text(
        '${ProductTypeEmojiDeserializer().call(productType)} ${ProductTypeDeserializer().call(productType)}',
        style: GoogleFonts.montserrat(
          fontSize: 14,
          fontWeight: FontWeight.w500,
          color: active ? Colors.white : Colors.black,
        ),
      ),
    );
  }
}
