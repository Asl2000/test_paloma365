# Test Paloma365

Приложение сделано в рамках тестового задания компании Paloma365

## Resources

- [Скачать APK](https://gitlab.com/Asl2000/test_paloma365/-/tree/main/assets/apk/app-release.apk)

## Stack

- DI (GetIt)
- State manager (Redux)
- Local Storage (Drift, sqlite3)

## Функции

- Страница просмотра столов
- Страница стола для заказа
- Фильтр блюд по типам
- Локальное хранилище
